package com.mobilike.carbon.videoplayer;

import android.os.Handler;

import java.util.concurrent.TimeUnit;

final class TimerRunnable implements Runnable {

    static final long TIMER_PERIOD = TimeUnit.SECONDS.toMillis(1);

    interface Listener {
        void onPeriod();
    }

    private Listener listener;
    private long seconds;

    private final Handler timerHandler;
    private long periodInSeconds;

    TimerRunnable(Handler timerHandler, long seconds, int periodInMinutes, Listener listener) {
        this.timerHandler = timerHandler;
        this.seconds = seconds;
        this.listener = listener;
        if (periodInMinutes > 0) {
            periodInSeconds = TimeUnit.MINUTES.toSeconds(periodInMinutes);
        }
    }

    boolean canRun() {
        return periodInSeconds > 0;
    }

    @Override
    public void run() {
        if (!canRun()) {
            // Remove any callbacks if present
            timerHandler.removeCallbacks(this);
            return;
        }

        seconds++;
        if (seconds == periodInSeconds) {
            if (listener != null) {
                listener.onPeriod();
            }

            // Reset timer
            seconds = 0L;
        }

        timerHandler.postDelayed(this, TIMER_PERIOD);
    }

    void destroy() {
        listener = null;
    }
}