package com.mobilike.carbon.videoplayer;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.ads.interactivemedia.v3.api.player.ContentProgressProvider;
import com.google.ads.interactivemedia.v3.api.player.VideoAdPlayer;
import com.google.ads.interactivemedia.v3.api.player.VideoProgressUpdate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mertkanuzunparmak on 13/04/16.
 * mertkan@mobilike.com
 */
public class CarbonVideoPlayerWithAdPlayback extends RelativeLayout {

    public interface OnContentCompleteListener {
        void onContentComplete();
    }

    CarbonVideoPlayer carbonVideoPlayer;
    private ViewGroup adUiContainer;
    private ProgressBar progressBar;
    private String contentVideoUrl;

    boolean isAdPlaybackStarted;
    private int savedVideoPosition;

    OnContentCompleteListener onContentCompleteListener;
    private ContentProgressProvider contentProgressProvider;

    private VideoAdPlayer videoAdPlayer;
    final List<VideoAdPlayer.VideoAdPlayerCallback> adCallbacks = new ArrayList<>();

    public CarbonVideoPlayerWithAdPlayback(Context context) {
        super(context);
    }

    public CarbonVideoPlayerWithAdPlayback(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CarbonVideoPlayerWithAdPlayback(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        init();
    }

    private void init() {
        isAdPlaybackStarted = false;
        savedVideoPosition = 0;
        carbonVideoPlayer = (CarbonVideoPlayer) findViewById(R.id.carbon_video_view);
        adUiContainer = (ViewGroup) findViewById(R.id.carbon_video_ad_ui_container);
        progressBar = (ProgressBar) findViewById(R.id.carbon_progress_bar);

        videoAdPlayer = new VideoAdPlayer() {
            @Override
            public void playAd() {
                isAdPlaybackStarted = true;
                carbonVideoPlayer.play();
            }

            @Override
            public void loadAd(String url) {
                isAdPlaybackStarted = true;
                carbonVideoPlayer.setVideoPath(url);
            }

            @Override
            public void stopAd() {
                carbonVideoPlayer.stopPlayback();
            }

            @Override
            public void pauseAd() {
                carbonVideoPlayer.pause();
            }

            @Override
            public void resumeAd() {
                playAd();
            }

            @Override
            public void addCallback(VideoAdPlayerCallback videoAdPlayerCallback) {
                adCallbacks.add(videoAdPlayerCallback);
            }

            @Override
            public void removeCallback(VideoAdPlayerCallback videoAdPlayerCallback) {
                adCallbacks.remove(videoAdPlayerCallback);
            }

            @Override
            public VideoProgressUpdate getAdProgress() {
                if (!isAdPlaybackStarted || carbonVideoPlayer.getDuration() <= 0) {
                    return VideoProgressUpdate.VIDEO_TIME_NOT_READY;
                }
                return new VideoProgressUpdate(carbonVideoPlayer.getCurrentPosition(),
                        carbonVideoPlayer.getDuration());
            }
        };

        contentProgressProvider = new ContentProgressProvider() {
            @Override
            public VideoProgressUpdate getContentProgress() {
                if (isAdPlaybackStarted || carbonVideoPlayer.getDuration() <= 0) {
                    return VideoProgressUpdate.VIDEO_TIME_NOT_READY;
                }
                return new VideoProgressUpdate(carbonVideoPlayer.getCurrentPosition(),
                        carbonVideoPlayer.getDuration());
            }
        };

        carbonVideoPlayer.addPlayerCallback(new CarbonVideoPlayer.PlayerCallback() {
            @Override
            public void onPlay() {
                if (isAdPlaybackStarted) {
                    for (VideoAdPlayer.VideoAdPlayerCallback callback : adCallbacks) {
                        callback.onPlay();
                    }
                }
            }

            @Override
            public void onPause() {
                if (isAdPlaybackStarted) {
                    for (VideoAdPlayer.VideoAdPlayerCallback callback : adCallbacks) {
                        callback.onPause();
                    }
                }
            }

            @Override
            public void onResume() {
                if (isAdPlaybackStarted) {
                    for (VideoAdPlayer.VideoAdPlayerCallback callback : adCallbacks) {
                        callback.onResume();
                    }
                }
            }

            @Override
            public void onError() {
                if (isAdPlaybackStarted) {
                    for (VideoAdPlayer.VideoAdPlayerCallback callback : adCallbacks) {
                        callback.onError();
                    }
                }
            }

            @Override
            public void onCompleted() {
                if (isAdPlaybackStarted) {
                    for (VideoAdPlayer.VideoAdPlayerCallback callback : adCallbacks) {
                        callback.onEnded();
                    }
                } else {
                    // Alert an external listener that our content video is complete.
                    if (onContentCompleteListener != null) {
                        onContentCompleteListener.onContentComplete();
                    }
                }
            }

            @Override
            public void onProgressChanged(int currentPosition) {
                // no-op
            }
        });
    }

    @Override
    protected void onDetachedFromWindow() {
        carbonVideoPlayer.removePlayerCallbacks();
        super.onDetachedFromWindow();
    }

    public void setOnContentCompleteListener(OnContentCompleteListener onContentCompleteListener) {
        this.onContentCompleteListener = onContentCompleteListener;
    }

    public void setContentVideoUrl(String contentVideoUrl) {
        this.contentVideoUrl = contentVideoUrl;
    }

    public void savePosition() {
        if (isAdPlaybackStarted) {
            savedVideoPosition = (int) carbonVideoPlayer.getCurrentPosition();
        }
    }

    public void restorePosition() {
        if (isAdPlaybackStarted && savedVideoPosition != 0) {
            carbonVideoPlayer.seekTo(savedVideoPosition);
        }
    }

    public void pauseContentForAdPlayback() {
        savePosition();
        carbonVideoPlayer.stopPlayback();
    }

    public void resumeContentAfterAdPlayback(int contentVideoPosition) {
        if (contentVideoUrl == null || contentVideoUrl.isEmpty()) {
            return;
        }

        isAdPlaybackStarted = true;
        carbonVideoPlayer.setVideoPath(contentVideoUrl);
        if (contentVideoPosition != 0) {
            carbonVideoPlayer.seekTo(contentVideoPosition);
        }
        carbonVideoPlayer.play();
    }

    public void pauseContentBeforeOnPause() {
        savePosition();
        carbonVideoPlayer.pause();
    }

    public void resumeContentAfterOnResume() {
        restorePosition();
        carbonVideoPlayer.play();
    }

    public ViewGroup getAdUiContainer() {
        return adUiContainer;
    }

    public VideoAdPlayer getVideoAdPlayer() {
        return videoAdPlayer;
    }

    public CarbonVideoPlayer getCarbonVideoPlayer() {
        return carbonVideoPlayer;
    }

    public boolean isAdPlaybackStarted() {
        return isAdPlaybackStarted;
    }

    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    public void setVideoVisibility(boolean visible) {
        carbonVideoPlayer.setVideoVisibility(visible);
    }

    public ContentProgressProvider getContentProgressProvider() {
        return contentProgressProvider;
    }
}
