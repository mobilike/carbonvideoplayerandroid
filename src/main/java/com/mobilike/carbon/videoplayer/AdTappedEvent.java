package com.mobilike.carbon.videoplayer;

/**
 * Created by selcukbulca on 15/04/16.
 */
final class AdTappedEvent {

    private final String clickThroughUrl;

    AdTappedEvent(String clickThroughUrl) {
        this.clickThroughUrl = clickThroughUrl;
    }

    String getClickThroughUrl() {
        return clickThroughUrl;
    }
}
