package com.mobilike.carbon.videoplayer;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by selcukbulca on 26/09/2016.
 */
class FetchRemoteVideoConfigTask extends AsyncTask<Void, Void, Boolean> {

    private final OkHttpClient client;
    private final String packageName;
    private String version;

    FetchRemoteVideoConfigTask(Context context) {
        client = new OkHttpClient();
        packageName = context.getPackageName();

        try {
            version = context.getPackageManager().getPackageInfo(packageName, 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            version = "invalid";
        }
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        HttpUrl url = HttpUrl.parse(BuildConfig.CARBON_PLAYER_CONFIG_URL)
                .newBuilder()
                .addQueryParameter("packageName", packageName)
                .addQueryParameter("version", version)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .build();

        try {
            Response response = client.newCall(request).execute();
            JSONObject object = new JSONObject(response.body().string());
            return object.getBoolean("useDefaultImaPlayer");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(Boolean useDefaultImaPlayer) {
        if (useDefaultImaPlayer != null) {
            CarbonVideoPlayerManager.internal().setUseDefaultImaPlayer(useDefaultImaPlayer);
        }
    }
}
