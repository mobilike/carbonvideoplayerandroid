package com.mobilike.carbon.videoplayer;

import android.net.Uri;
import android.support.annotation.Nullable;

import java.util.List;

/**
 * Created by selcukbulca on 04/08/2016.
 */
final class VideoAdUtils {

    private static final String KEY_DESCRIPTION_URL = "description_url";

    @Nullable
    static VideoAd getVideoAdForType(List<VideoAd> videoAds, VideoAdType adType) {
        if (videoAds == null || videoAds.isEmpty()) return null;

        for (VideoAd videoAd: videoAds) {
            if (videoAd.getType() == adType) {
                return videoAd;
            }
        }

        return null;
    }

    static String replaceDescriptionUrl(String publisherTag, String descriptionUrl) {
        if (publisherTag == null) {
            return null;
        }

        if (descriptionUrl == null) {
            return publisherTag;
        }

        // Uri does not provide a way to change query parameter value.
        // So here is a ugly way to do it.
        Uri uri = Uri.parse(publisherTag);
        if (uri.getQueryParameter(KEY_DESCRIPTION_URL) != null) {
            String oldDescriptionUrl = uri.getQueryParameter(KEY_DESCRIPTION_URL);
            return uri.toString().replace(oldDescriptionUrl, Uri.encode(descriptionUrl));
        }

        return uri.toString();
    }
}
