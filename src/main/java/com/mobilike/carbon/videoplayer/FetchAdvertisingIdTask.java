package com.mobilike.carbon.videoplayer;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import java.lang.ref.WeakReference;

/**
 * Created by selcukbulca on 26/09/2016.
 */
final class FetchAdvertisingIdTask extends AsyncTask<Void, Void, String> {

    private final WeakReference<Context> contextRef;

    FetchAdvertisingIdTask(Context context) {
        this.contextRef = new WeakReference<>(context);
    }

    @Override
    protected String doInBackground(Void... params) {
        if (contextRef.get() == null) {
            return null;
        }

        return AdvertisingIdManager.getAdvertisingId(contextRef.get());
    }

    @Override
    protected void onPostExecute(String advertisingId) {
        if (TextUtils.isEmpty(advertisingId)) {
            return;
        }

        CarbonVideoPlayerManager.internal().saveAdvertisingId(advertisingId);
        contextRef.clear();
    }
}
