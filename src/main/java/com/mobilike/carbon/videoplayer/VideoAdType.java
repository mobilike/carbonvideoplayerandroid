package com.mobilike.carbon.videoplayer;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by selcukbulca on 04/08/2016.
 */
enum VideoAdType implements Parcelable {
    PRE_ROLL,
    MID_ROLL,
    POST_ROLL;

    static final VideoAdType[] VALUES = values();

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ordinal());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VideoAdType> CREATOR = new Creator<VideoAdType>() {
        @Override
        public VideoAdType createFromParcel(Parcel in) {
            return VALUES[in.readInt()];
        }

        @Override
        public VideoAdType[] newArray(int size) {
            return new VideoAdType[size];
        }
    };
}
