package com.mobilike.carbon.videoplayer;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by selcukbulca on 04/08/2016.
 */
final class VideoAd implements Parcelable {

    private final VideoAdType type;
    private final String publisherTag;
    private final int period;

    VideoAd(VideoAdType type, String publisherTag) {
        this(type, publisherTag, 0);
    }

    VideoAd(VideoAdType type, String publisherTag, int period) {
        this.type = type;
        this.publisherTag = publisherTag;
        this.period = period;
    }

    VideoAdType getType() {
        return type;
    }

    String getPublisherTag() {
        return publisherTag;
    }

    int getPeriod() {
        return period;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(type, flags);
        dest.writeString(this.publisherTag);
        dest.writeInt(this.period);
    }

    VideoAd(Parcel in) {
        this.type = in.readParcelable(VideoAdType.class.getClassLoader());
        this.publisherTag = in.readString();
        this.period = in.readInt();
    }

    public static final Creator<VideoAd> CREATOR = new Creator<VideoAd>() {
        @Override
        public VideoAd createFromParcel(Parcel source) {
            return new VideoAd(source);
        }

        @Override
        public VideoAd[] newArray(int size) {
            return new VideoAd[size];
        }
    };
}
