package com.mobilike.carbon.videoplayer;

import android.content.Context;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by selcukbulca on 26/09/2016.
 */
final class AdvertisingIdManager {

    private AdvertisingIdManager() {
        // no instance
    }

    static String getAdvertisingId(Context context) {
        AdvertisingIdClient.Info adInfo;

        try {
            adInfo = AdvertisingIdClient.getAdvertisingIdInfo(context);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
            return null;
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
            return null;
        }

        return sha1(adInfo.getId());
    }

    private static String sha1(String advertisingId) {
        if (advertisingId == null) {
            return null;
        }

        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
            byte[] digestResultArray = messageDigest.digest(advertisingId.getBytes());
            StringBuilder stringBuilder = new StringBuilder();
            for (byte digestResult : digestResultArray) {
                stringBuilder.append(Integer.toString((digestResult & 0xff) + 0x100, 16).substring(1));
            }

            return stringBuilder.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
}
