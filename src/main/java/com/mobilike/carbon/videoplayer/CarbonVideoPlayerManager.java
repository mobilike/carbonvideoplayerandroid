package com.mobilike.carbon.videoplayer;

import android.content.Context;
import android.support.annotation.StringRes;

/**
 * Created by selcukbulca on 26/09/2016.
 */
public final class CarbonVideoPlayerManager {

    private static CarbonVideoPlayerManagerInternal internalInstance;

    static CarbonVideoPlayerManagerInternal internal() {
        return internalInstance;
    }

    public static void init(Context context, @StringRes int interstitialAdUnitIdRes) {
        init(context, context.getString(interstitialAdUnitIdRes));
    }

    public static void init(Context context, String interstitialAdUnitId) {
        internalInstance = new CarbonVideoPlayerManagerInternal(context);
        internalInstance.setInterstitialAdUnitId(interstitialAdUnitId);

        if (internalInstance.getAdvertisingId() == null) {
            new FetchAdvertisingIdTask(context).execute();
        }

        new FetchRemoteVideoConfigTask(context).execute();
    }

    public static void setInterstitialAdUnitId(String adUnitId) {
        internalInstance.setInterstitialAdUnitId(adUnitId);
    }
}
