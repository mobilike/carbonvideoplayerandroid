package com.mobilike.carbon.videoplayer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Locale;

/**
 * Created by mertkanuzunparmak on 13/04/16.
 * mertkan@mobilike.com
 */
public class CarbonVideoFragment extends Fragment {

    private CarbonVideoPlayerController carbonVideoPlayerController;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_carbon_video, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (carbonVideoPlayerController != null) {
            carbonVideoPlayerController.onResume();
        }
    }

    @Override
    public void onPause() {
        if (carbonVideoPlayerController != null) {
            carbonVideoPlayerController.onPause();
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (carbonVideoPlayerController != null) {
            carbonVideoPlayerController.onDestroy();
        }
        super.onDestroy();
    }

    private void initUi(View rootView) {
        CarbonVideoPlayerWithAdPlayback callback =
                (CarbonVideoPlayerWithAdPlayback) rootView.findViewById(R.id.carbon_video_player_with_ad_playback);

        carbonVideoPlayerController = new CarbonVideoPlayerController.Builder(getActivity(), rootView)
                .videoPlayerWithAdPlayback(callback)
                .language(Locale.getDefault().getLanguage())
                .build();
    }

    public void loadVideo(CarbonVideoConfig videoConfig) {
        carbonVideoPlayerController.setContentVideo(videoConfig.getVideoUrl());
        carbonVideoPlayerController.setVideoAds(videoConfig.getVideoAds());
        carbonVideoPlayerController.setCustomParams(videoConfig.getCustomParams());

        VideoAd preRoll = VideoAdUtils.getVideoAdForType(videoConfig.getVideoAds(), VideoAdType.PRE_ROLL);
        carbonVideoPlayerController.requestAndPlayAds(preRoll, true);
    }

    public boolean isAdPlaying() {
        return carbonVideoPlayerController != null && carbonVideoPlayerController.isAdPlaying();
    }
}
