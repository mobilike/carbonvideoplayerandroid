package com.mobilike.carbon.videoplayer;

import android.support.annotation.IdRes;

import java.util.Arrays;
import java.util.List;

/**
 * Created by selcukbulca on 19/04/16.
 */
enum CarbonCompanion {

    TOP_LEFT(R.id.carbon_video_companion_ad_slot_top_left, 170, 120),
    TOP_RIGHT(R.id.carbon_video_companion_ad_slot_top_right, 130, 35),
    BOTTOM_LEFT(R.id.carbon_video_companion_ad_slot_bottom_left, 170, 130);

    static final List<CarbonCompanion> VALUES = Arrays.asList(TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT);

    @IdRes
    private final int layoutId;
    private final int width;
    private final int height;

    CarbonCompanion(@IdRes int layoutId, int width, int height) {
        this.layoutId = layoutId;
        this.width = width;
        this.height = height;
    }

    int getLayoutId() {
        return layoutId;
    }

    int getWidth() {
        return width;
    }

    int getHeight() {
        return height;
    }
}
