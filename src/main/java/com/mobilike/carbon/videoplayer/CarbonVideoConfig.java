package com.mobilike.carbon.videoplayer;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by selcukbulca on 25/04/16.
 */
public final class CarbonVideoConfig implements Parcelable {

    private final String videoUrl;
    private final String videoShareUrl;
    private final String preRollTag;
    private final String midRollTag;
    private final int midRollPeriodInMinutes;
    private final String postRollTag;
    private final Map<String, String> customParams;

    private ArrayList<VideoAd> videoAds;

    public static Builder builder(String videoUrl) {
        return new Builder(videoUrl);
    }

    CarbonVideoConfig(Builder builder) {
        videoUrl = builder.videoUrl;
        videoShareUrl = builder.videoShareUrl;
        preRollTag = builder.preRollTag;
        midRollTag = builder.midRollTag;
        midRollPeriodInMinutes = builder.midRollPeriodInMinutes;
        postRollTag = builder.postRollTag;
        customParams = builder.customParams;
    }

    String getVideoUrl() {
        return videoUrl;
    }

    private String getPreRollTag() {
        return VideoAdUtils.replaceDescriptionUrl(preRollTag, videoShareUrl);
    }

    private String getMidRollTag() {
        return VideoAdUtils.replaceDescriptionUrl(midRollTag, videoShareUrl);
    }

    private String getPostRollTag() {
        return VideoAdUtils.replaceDescriptionUrl(postRollTag, videoShareUrl);
    }

    List<VideoAd> getVideoAds() {
        if (videoAds == null) {
            videoAds = new ArrayList<>(3);
            videoAds.add(new VideoAd(VideoAdType.PRE_ROLL, getPreRollTag()));
            videoAds.add(new VideoAd(VideoAdType.MID_ROLL, getMidRollTag(), midRollPeriodInMinutes));
            videoAds.add(new VideoAd(VideoAdType.POST_ROLL, getPostRollTag()));
        }

        return videoAds;
    }

    Map<String, String> getCustomParams() {
        return customParams;
    }

    public static final class Builder {

        String videoUrl;
        String videoShareUrl;
        String preRollTag;
        String midRollTag;
        int midRollPeriodInMinutes;
        String postRollTag;
        Map<String, String> customParams;

        public Builder(String videoUrl) {
            this.videoUrl = videoUrl;
        }

        public Builder videoShareUrl(String videoShareUrl) {
            this.videoShareUrl = videoShareUrl;
            return this;
        }

        public Builder preRollTag(String preRollTag) {
            this.preRollTag = preRollTag;
            return this;
        }

        public Builder midRollTag(String midRollTag, int midRollPeriodInMinutes) {
            this.midRollTag = midRollTag;
            this.midRollPeriodInMinutes = midRollPeriodInMinutes;
            return this;
        }

        public Builder postRollTag(String postRollTag) {
            this.postRollTag = postRollTag;
            return this;
        }

        public Builder customParams(Map<String, String> customParams) {
            this.customParams = customParams;
            return this;
        }

        public CarbonVideoConfig build() {
            return new CarbonVideoConfig(this);
        }
    }

    //region Parcelable implementation
    CarbonVideoConfig(Parcel in) {
        videoUrl = in.readString();
        videoShareUrl = in.readString();
        preRollTag = in.readString();
        midRollTag = in.readString();
        midRollPeriodInMinutes = in.readInt();
        postRollTag = in.readString();
        videoAds = in.createTypedArrayList(VideoAd.CREATOR);

        customParams = new HashMap<>();
        int count = in.readInt();
        for (int i = 0; i < count; i++) {
            customParams.put(in.readString(), in.readString());
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(videoUrl);
        dest.writeString(videoShareUrl);
        dest.writeString(preRollTag);
        dest.writeString(midRollTag);
        dest.writeInt(midRollPeriodInMinutes);
        dest.writeString(postRollTag);
        dest.writeTypedList(videoAds);

        if (customParams == null) {
            dest.writeInt(0);
            return;
        }

        int count = customParams.size();
        dest.writeInt(count);


        for (Map.Entry<String, String> entry : customParams.entrySet()) {
            dest.writeString(entry.getKey());
            dest.writeString(entry.getValue());
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CarbonVideoConfig> CREATOR = new Creator<CarbonVideoConfig>() {
        @Override
        public CarbonVideoConfig createFromParcel(Parcel in) {
            return new CarbonVideoConfig(in);
        }

        @Override
        public CarbonVideoConfig[] newArray(int size) {
            return new CarbonVideoConfig[size];
        }
    };
    //endregion
}
