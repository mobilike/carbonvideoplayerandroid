package com.mobilike.carbon.videoplayer;

import android.net.Uri;

/**
 * Created by selcukbulca on 07/09/2016.
 */
final class SafeUriBuilder {

    private final Uri.Builder builder;

    SafeUriBuilder() {
        this(new Uri.Builder());
    }

    SafeUriBuilder(Uri.Builder builder) {
        this.builder = builder;
    }

    SafeUriBuilder appendQueryParameter(String key, String value) {
        if (key == null || value == null) {
            return this;
        }

        builder.appendQueryParameter(key, value);
        return this;
    }

    @Override
    public String toString() {
        String uri = builder.toString();
        if (uri.length() <= 0) {
            return uri;
        }

        if (uri.charAt(0) == '?') {
            uri = uri.substring(1);
        }

        return uri;
    }
}
