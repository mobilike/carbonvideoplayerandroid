package com.mobilike.carbon.videoplayer;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

/**
 * Created by selcukbulca on 26/09/2016.
 */
class CarbonVideoPlayerManagerInternal {

    private static final String KEY_ADVERTISING_ID = "advertising_id";

    private String interstitialAdUnitId;
    private boolean useDefaultImaPlayer;

    private final SharedPreferences sharedPrefs;

    CarbonVideoPlayerManagerInternal(Context context) {
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    void saveAdvertisingId(String advertisingId) {
        sharedPrefs.edit()
                .putString(KEY_ADVERTISING_ID, advertisingId)
                .apply();
    }

    @Nullable
    String getAdvertisingId() {
        return sharedPrefs.getString(KEY_ADVERTISING_ID, null);
    }

    void setInterstitialAdUnitId(String interstitialAdUnitId) {
        this.interstitialAdUnitId = interstitialAdUnitId;
    }

    String getInterstitialAdUnitId() {
        return interstitialAdUnitId;
    }

    void setUseDefaultImaPlayer(boolean useDefaultImaPlayer) {
        this.useDefaultImaPlayer = useDefaultImaPlayer;
    }

    boolean useDefaultImaPlayer() {
        return useDefaultImaPlayer;
    }
}
