package com.mobilike.carbon.videoplayer;

import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.google.ads.interactivemedia.v3.api.Ad;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Field;

/**
 * Created by selcukbulca on 19/04/16.
 */
abstract class CarbonAdEventHelper {

    private static final String AD_SYSTEM_GDFP = "GDFP";

    static CarbonAdEventHelper create(@Nullable Ad ad) {
        if (ad != null
                && !CarbonVideoPlayerManager.internal().useDefaultImaPlayer()
                && isDfpAd(ad)) {
            return new CarbonAdEventHelperHacky(ad);
        } else {
            return new CarbonAdEventHelperDefault(ad);
        }
    }

    private static boolean isDfpAd(Ad ad) {
        boolean adWrapperIsDfp = false;
        String[] adWrapperSystems = ad.getAdWrapperSystems();
        if (adWrapperSystems != null) {
            for (String adWrapperSystem : adWrapperSystems) {
                if (AD_SYSTEM_GDFP.equalsIgnoreCase(adWrapperSystem)) {
                    adWrapperIsDfp = true;
                }
            }
        }

        boolean adSystemIsDfp = AD_SYSTEM_GDFP.equalsIgnoreCase(ad.getAdSystem());

        return adWrapperIsDfp || adSystemIsDfp;
    }

    @Nullable
    private final Ad ad;

    CarbonAdEventHelper(@Nullable Ad ad) {
        this.ad = ad;
    }

    boolean hasSameAd(Ad ad) {
        return this.ad != null && this.ad.equals(ad);
    }

    abstract void onAdStarted(Ad ad, ViewGroup container);

    abstract void onAdTapped(Ad ad);

    private static final class CarbonAdEventHelperDefault extends CarbonAdEventHelper {

        CarbonAdEventHelperDefault(@Nullable Ad ad) {
            super(ad);
        }

        @Override
        void onAdStarted(Ad ad, ViewGroup container) {
            // no-op
        }

        @Override
        void onAdTapped(Ad ad) {
            // no-op
        }
    }

    private static final class CarbonAdEventHelperHacky extends CarbonAdEventHelper {

        CarbonAdEventHelperHacky(@Nullable Ad ad) {
            super(ad);
        }

        @Override
        void onAdStarted(Ad ad, ViewGroup container) {
            adjustContainer(container);
        }

        private void adjustContainer(ViewGroup container) {
            if (container == null) {
                return;
            }

            for (int i = 0; i < container.getChildCount(); i++) {
                if (container.getChildAt(i) instanceof WebView) {
                    WebView webView = (WebView) container.getChildAt(i);

                    // Make "learn more" invisible by adding top margin
                    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) webView.getLayoutParams();
                    int topMargin = container.getResources().getDimensionPixelSize(R.dimen.carbon_web_view_top_margin);
                    marginLayoutParams.topMargin = -topMargin;
                    webView.setLayoutParams(marginLayoutParams);
                }
            }
        }

        @Override
        void onAdTapped(Ad ad) {
            EventBus.getDefault().post(new AdTappedEvent(getClickThroughUrl(ad)));
        }

        @SuppressWarnings("TryWithIdenticalCatches")
        private String getClickThroughUrl(Ad ad) {
            Field field;
            String clickThroughUrl = "";
            try {
                if (ad != null) {
                    field = ad.getClass().getDeclaredField("clickThroughUrl");
                    if (field != null) {
                        field.setAccessible(true);
                        clickThroughUrl = (String) field.get(ad);
                    }
                }
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            return clickThroughUrl;
        }
    }
}
