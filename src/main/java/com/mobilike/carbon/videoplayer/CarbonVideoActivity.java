package com.mobilike.carbon.videoplayer;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by tekinarslan on 10/19/15.
 */
public class CarbonVideoActivity extends AppCompatActivity {

    private static final String KEY_VIDEO_CONFIG = "video_config";

    public static void start(Context context, CarbonVideoConfig videoConfig) {
        Intent intent = new Intent(context, CarbonVideoActivity.class);
        intent.putExtra(KEY_VIDEO_CONFIG, videoConfig);
        context.startActivity(intent);
    }

    private CarbonVideoConfig videoConfig;
    private CarbonVideoFragment videoFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carbon_video);

        Bundle extras = getIntent().getExtras();
        videoConfig = extras.getParcelable(KEY_VIDEO_CONFIG);
        if (videoConfig == null) return;

        playVideo();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Make video full screen
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (videoFragment != null && videoFragment.isAdPlaying()) return;
        super.onBackPressed();
    }

    protected final void playVideo() {
        videoFragment = (CarbonVideoFragment)
                getSupportFragmentManager().findFragmentById(R.id.carbon_video_fragment);
        videoFragment.loadVideo(videoConfig);
    }

    @Subscribe
    public void onAdTappedEvent(AdTappedEvent event) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(event.getClickThroughUrl()));
        startActivity(browserIntent);
    }

    @Subscribe
    public void onShowFallbackInterstitialEvent(ShowFallbackInterstitialEvent event) {
        final PublisherInterstitialAd interstitialAd = new PublisherInterstitialAd(this);
        interstitialAd.setAdUnitId(CarbonVideoPlayerManager.internal().getInterstitialAdUnitId());
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                EventBus.getDefault().post(new ResumeContentVideoEvent());
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                EventBus.getDefault().post(new ResumeContentVideoEvent());
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                interstitialAd.show();
            }
        });

        PublisherAdRequest.Builder adRequestBuilder = new PublisherAdRequest.Builder();
        adRequestBuilder.setPublisherProvidedId(CarbonVideoPlayerManager.internal().getAdvertisingId());
        interstitialAd.loadAd(adRequestBuilder.build());
    }
}
