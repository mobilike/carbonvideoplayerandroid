package com.mobilike.carbon.videoplayer;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;

import com.devbrackets.android.exomedia.EMVideoView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mertkanuzunparmak on 13/04/16.
 * mertkan@mobilike.com
 */
public class CarbonVideoView extends EMVideoView implements CarbonVideoPlayer {

    private enum PlaybackState {
        STOPPED,
        PAUSED,
        PLAYING
    }

    PlaybackState playbackState;
    final List<PlayerCallback> playerCallbacks = new ArrayList<>();

    public CarbonVideoView(Context context) {
        this(context, null);
    }

    public CarbonVideoView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CarbonVideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        if (!isInEditMode()) {
            init();
        }
    }

    private void init() {
        playbackState = PlaybackState.STOPPED;

        super.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                if (mediaPlayer != null) {
                    mediaPlayer.reset();
                }

                playbackState = PlaybackState.STOPPED;

                for (PlayerCallback callback : playerCallbacks) {
                    callback.onCompleted();
                }
            }
        });

        super.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
                playbackState = PlaybackState.STOPPED;
                for (PlayerCallback callback : playerCallbacks) {
                    callback.onError();
                }

                return true;
            }
        });

        super.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void play() {
        start();
    }

    @Override
    public void start() {
        super.start();
        switch (playbackState) {
            case STOPPED:
                for (PlayerCallback callback : playerCallbacks) {
                    callback.onPlay();
                }
                break;
            case PAUSED:
                for (PlayerCallback callback : playerCallbacks) {
                    callback.onResume();
                }
                break;
            default:
                // Already playing; do nothing.
                break;
        }
        playbackState = PlaybackState.PLAYING;
    }

    @Override
    public void pause() {
        super.pause();
        playbackState = PlaybackState.PAUSED;
        for (PlayerCallback callback : playerCallbacks) {
            callback.onPause();
        }
    }

    @Override
    public void stopPlayback() {
        super.stopPlayback();
        playbackState = PlaybackState.STOPPED;
    }

    @Override
    public void setVideoURI(Uri uri) {
        super.setVideoURI(uri);
        if (defaultControls != null) {
            // Do not show default progress bar
            defaultControls.setLoading(false);
        }
    }

    @Override
    public void addPlayerCallback(PlayerCallback callback) {
        playerCallbacks.add(callback);
    }

    @Override
    public void removePlayerCallbacks() {
        playerCallbacks.clear();
    }

    @Override
    public void setVideoVisibility(boolean visible) {
        setVisibility(visible ? View.VISIBLE : View.GONE);
    }
}
