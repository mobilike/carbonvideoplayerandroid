package com.mobilike.carbon.videoplayer;

import com.devbrackets.android.exomedia.listener.ExoPlayerListener;

/**
 * Created by mertkanuzunparmak on 13/04/16.
 * mertkan@mobilike.com
 */
interface CarbonVideoPlayer {

    interface PlayerCallback {
        void onPlay();

        void onPause();

        void onResume();

        void onCompleted();

        void onError();

        void onProgressChanged(int currentPosition);
    }

    void play();

    void pause();

    void seekTo(int videoPosition);

    void stopPlayback();

    boolean isPlaying();

    void setDefaultControlsEnabled(boolean defaultControlsEnabled);

    void setVideoPath(String videoUrl);

    void addPlayerCallback(PlayerCallback callback);

    void removePlayerCallbacks();

    void addExoPlayerListener(ExoPlayerListener listener);

    void setVideoVisibility(boolean visible);

    long getCurrentPosition();

    long getDuration();
}
