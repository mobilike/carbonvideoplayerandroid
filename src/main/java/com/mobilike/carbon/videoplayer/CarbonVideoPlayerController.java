package com.mobilike.carbon.videoplayer;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.devbrackets.android.exomedia.listener.ExoPlayerListener;
import com.google.ads.interactivemedia.v3.api.Ad;
import com.google.ads.interactivemedia.v3.api.AdDisplayContainer;
import com.google.ads.interactivemedia.v3.api.AdError;
import com.google.ads.interactivemedia.v3.api.AdErrorEvent;
import com.google.ads.interactivemedia.v3.api.AdEvent;
import com.google.ads.interactivemedia.v3.api.AdsLoader;
import com.google.ads.interactivemedia.v3.api.AdsManager;
import com.google.ads.interactivemedia.v3.api.AdsManagerLoadedEvent;
import com.google.ads.interactivemedia.v3.api.AdsRenderingSettings;
import com.google.ads.interactivemedia.v3.api.AdsRequest;
import com.google.ads.interactivemedia.v3.api.CompanionAdSlot;
import com.google.ads.interactivemedia.v3.api.ImaSdkFactory;
import com.google.ads.interactivemedia.v3.api.ImaSdkSettings;
import com.google.android.exoplayer.ExoPlayer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

/**
 * Created by mertkanuzunparmak on 13/04/16.
 * mertkan@mobilike.com
 */
final class CarbonVideoPlayerController implements AdsLoader.AdsLoadedListener, AdErrorEvent.AdErrorListener, AdEvent.AdEventListener {

    private static final String TAG = CarbonVideoPlayerController.class.getSimpleName();

    private static final boolean AT_LEAST_LOLLIPOP = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    private static final List<String> MIME_TYPES = asList("video/mp4", "video/3gpp", "audio/mpeg", "audio/mp4");

    private AdDisplayContainer adDisplayContainer;
    AdsLoader adsLoader;
    private AdsManager adsManager;
    private AdsRenderingSettings adsRenderingSettings;
    private ImaSdkFactory imaSdkFactory;
    private CarbonAdEventHelper carbonAdEventHelper;
    private List<VideoAd> videoAds;
    private Map<String, String> customParams;

    private Handler timerHandler = new Handler();
    private TimerRunnable timerRunnable;

    boolean isAdPlaying;
    private boolean isAdErrorOccurred;
    private boolean isPreRollRequest;
    private boolean isPostRollRequest;
    private boolean hasPostRollRequested; // :'(
    private int contentVideoPosition;
    int currentPlaybackState;

    final CarbonVideoPlayerWithAdPlayback carbonVideoPlayerWithAdPlayback;
    private final Context context;
    private final List<ViewGroup> companionViews;

    CarbonVideoPlayerController(Builder builder) {
        context = builder.context;
        carbonVideoPlayerWithAdPlayback = builder.carbonVideoPlayerWithAdPlayback;
        companionViews = builder.companionViews;

        isAdPlaying = false;

        createAdsLoader(builder);

        adsLoader.addAdErrorListener(this);
        adsLoader.addAdsLoadedListener(this);

        carbonVideoPlayerWithAdPlayback.setOnContentCompleteListener(new CarbonVideoPlayerWithAdPlayback.OnContentCompleteListener() {
            @Override
            public void onContentComplete() {
                adsLoader.contentComplete();
            }
        });

        carbonVideoPlayerWithAdPlayback.getCarbonVideoPlayer().addExoPlayerListener(new ExoPlayerListener() {
            @Override
            public void onStateChanged(boolean playWhenReady, int playbackState) {
                if (currentPlaybackState != playbackState) {
                    currentPlaybackState = playbackState;
                    onExoPlayerStateChanged(playbackState);
                }
            }

            @Override
            public void onError(Exception e) {
                // no-op
            }

            @Override
            public void onVideoSizeChanged(int width, int height, int unAppliedRotationDegrees, float pixelWidthHeightRatio) {
                if (isAdPlaying) {
                    carbonVideoPlayerWithAdPlayback.getAdUiContainer().setVisibility(View.VISIBLE);
                    carbonVideoPlayerWithAdPlayback.hideProgressBar();
                }

                carbonVideoPlayerWithAdPlayback.getCarbonVideoPlayer().setVideoVisibility(true);
            }
        });
    }

    private void createAdsLoader(Builder builder) {
        ImaSdkSettings imaSdkSettings = new ImaSdkSettings();
        imaSdkSettings.setLanguage(builder.language);
        imaSdkFactory = ImaSdkFactory.getInstance();
        adsLoader = imaSdkFactory.createAdsLoader(builder.context, imaSdkSettings);
        adsRenderingSettings = imaSdkFactory.createAdsRenderingSettings();
    }

    void onExoPlayerStateChanged(int playbackState) {
        boolean stateReady = false;
        switch (playbackState) {
            case ExoPlayer.STATE_BUFFERING:
            case ExoPlayer.STATE_PREPARING:
                carbonVideoPlayerWithAdPlayback.showProgressBar();
                break;
            case ExoPlayer.STATE_READY:
                carbonVideoPlayerWithAdPlayback.hideProgressBar();
                stateReady = true;
                break;
            case ExoPlayer.STATE_ENDED:
                if (hasPostRollRequested) {
                    finish();
                } else if (!isAdPlaying) {
                    VideoAd postRollAd = VideoAdUtils.getVideoAdForType(videoAds, VideoAdType.POST_ROLL);
                    if (postRollAd != null && !TextUtils.isEmpty(postRollAd.getPublisherTag())) {
                        requestAndPlayAds(postRollAd, false);
                        isPostRollRequest = true;
                    } else {
                        finish();
                    }
                }

                break;
        }

        if (timerRunnable == null) {
            return;
        }

        // If the ad is playing or the content is paused, then pause timer
        if (isAdPlaying || !carbonVideoPlayerWithAdPlayback.getCarbonVideoPlayer().isPlaying()) {
            timerHandler.removeCallbacks(timerRunnable);
            return;
        }

        if (stateReady && timerRunnable.canRun()) {
            timerHandler.postDelayed(timerRunnable, TimerRunnable.TIMER_PERIOD);
        } else {
            timerHandler.removeCallbacks(timerRunnable);
        }
    }

    void requestAndPlayAds(VideoAd videoAd, boolean preRollRequest) {
        isPreRollRequest = preRollRequest;
        requestAndPlayAds(videoAd.getPublisherTag());
    }

    private void requestAndPlayAds(String publisherTag) {
        if (TextUtils.isEmpty(publisherTag)) {
            onResumeContentRequested();
            return;
        }

        if (adsManager != null) {
            adsManager.destroy();
        }

        adsLoader.contentComplete();

        createAdDisplayContainer();
        createCompanionAdSlots();

        AdsRequest adsRequest = createAdsRequest(publisherTag);
        adsLoader.requestAds(adsRequest);
    }

    private AdsRequest createAdsRequest(String publisherTag) {
        AdsRequest adsRequest = imaSdkFactory.createAdsRequest();
        adsRequest.setAdTagUrl(buildVastUrl(publisherTag));
        adsRequest.setAdDisplayContainer(adDisplayContainer);
        adsRequest.setContentProgressProvider(carbonVideoPlayerWithAdPlayback.getContentProgressProvider());
        return adsRequest;
    }

    private String buildVastUrl(String publisherTag) {
        String customParameters = null;
        if (customParams != null) {
            SafeUriBuilder builder = new SafeUriBuilder();
            for (Map.Entry<String, String> entry : customParams.entrySet()) {
                builder.appendQueryParameter(entry.getKey(), entry.getValue());
            }

            customParameters = builder.toString();
        }

        Uri.Builder builder = Uri.parse(publisherTag)
                .buildUpon();

        return new SafeUriBuilder(builder)
                .appendQueryParameter("cust_params", customParameters)
                .appendQueryParameter("ppid", CarbonVideoPlayerManager.internal().getAdvertisingId())
                .toString();
    }

    private void createCompanionAdSlots() {
        List<CarbonCompanion> companions = CarbonCompanion.VALUES;
        List<CompanionAdSlot> companionAdSlots = new ArrayList<>(companions.size());

        for (int i = 0, size = companions.size(); i < size; i++) {
            CarbonCompanion companion = companions.get(i);

            CompanionAdSlot companionAdSlot = imaSdkFactory.createCompanionAdSlot();
            companionAdSlot.setContainer(companionViews.get(i));
            companionAdSlot.setSize(companion.getWidth(), companion.getHeight());

            companionAdSlots.add(companionAdSlot);
        }

        adDisplayContainer.setCompanionSlots(companionAdSlots);
    }

    private void createAdDisplayContainer() {
        adDisplayContainer = imaSdkFactory.createAdDisplayContainer();
        adDisplayContainer.setPlayer(carbonVideoPlayerWithAdPlayback.getVideoAdPlayer());
        adDisplayContainer.setAdContainer(carbonVideoPlayerWithAdPlayback.getAdUiContainer());
    }

    void onResume() {
        carbonVideoPlayerWithAdPlayback.restorePosition();
        if (adsManager != null && carbonVideoPlayerWithAdPlayback.isAdPlaybackStarted()) {
            adsManager.resume();
        } else {
            carbonVideoPlayerWithAdPlayback.resumeContentAfterOnResume();
        }
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    void onPause() {
        carbonVideoPlayerWithAdPlayback.savePosition();
        if (adsManager != null && carbonVideoPlayerWithAdPlayback.isAdPlaybackStarted()) {
            adsManager.pause();
        } else {
            carbonVideoPlayerWithAdPlayback.pauseContentBeforeOnPause();
        }

        timerHandler.removeCallbacks(timerRunnable);
    }

    void onDestroy() {
        if (timerRunnable != null) {
            timerRunnable.destroy();
        }

        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    void setVideoAds(List<VideoAd> videoAds) {
        this.videoAds = videoAds;
        final VideoAd midRollAd = VideoAdUtils.getVideoAdForType(videoAds, VideoAdType.MID_ROLL);
        if (midRollAd == null) return;

        timerRunnable = new TimerRunnable(timerHandler, 0L, midRollAd.getPeriod(), new TimerRunnable.Listener() {
            @Override
            public void onPeriod() {
                String midRollPublisherTag = midRollAd.getPublisherTag();
                if (!TextUtils.isEmpty(midRollPublisherTag)) {
                    requestAndPlayAds(midRollAd, false);
                }
            }
        });
    }

    void setContentVideo(String videoPath) {
        carbonVideoPlayerWithAdPlayback.setContentVideoUrl(videoPath);
    }

    void setCustomParams(Map<String, String> customParams) {
        this.customParams = customParams;
    }

    @Override
    public void onAdsManagerLoaded(AdsManagerLoadedEvent adsManagerLoadedEvent) {
        adsManager = adsManagerLoadedEvent.getAdsManager();

        adsManager.addAdErrorListener(this);
        adsManager.addAdEventListener(this);

        if (adsManager.getCurrentAd() != null) {
            Log.d(TAG, "Video Ad System: " + adsManager.getCurrentAd().getAdSystem());
        }

        if (AT_LEAST_LOLLIPOP) {
            adsManager.init();
        } else {
            adsRenderingSettings.setMimeTypes(MIME_TYPES);
            adsManager.init(adsRenderingSettings);
        }

    }

    boolean isAdPlaying() {
        return isAdPlaying;
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    @Override
    public void onAdError(AdErrorEvent adErrorEvent) {
        AdError adError = adErrorEvent.getError();
        adError.printStackTrace();
        Log.e(TAG, "Ad Error: " + adError.getMessage());
        // If post roll ad gives an error, than finish activity.
        if (isPostRollRequest) {
            finish();
            return;
        }

        // If any type of ads except pre roll gives an error, than
        // return immediately without showing fallback interstitial.
        if (!isPreRollRequest) {
            return;
        }

        if (!isAdErrorOccurred && adError.getErrorCode() == AdError.AdErrorCode.VAST_EMPTY_RESPONSE
                || adError.getErrorCode() == AdError.AdErrorCode.ADS_REQUEST_NETWORK_ERROR) {
            EventBus.getDefault().post(new ShowFallbackInterstitialEvent());
        }
        isAdErrorOccurred = true;
    }

    @Override
    public void onAdEvent(AdEvent adEvent) {
        Log.d(TAG, "Video Ad Event: " + adEvent.getType());

        Ad ad = adEvent.getAd();

        // Do not recreate carbonAdEventHelper
        if (carbonAdEventHelper == null || !carbonAdEventHelper.hasSameAd(ad)) {
            carbonAdEventHelper = CarbonAdEventHelper.create(ad);
        }

        switch (adEvent.getType()) {
            case CONTENT_RESUME_REQUESTED:
                onResumeContentRequested();
                break;
            case CONTENT_PAUSE_REQUESTED:
                onPauseContentRequested();
                break;
            case LOADED:
                onAdLoaded();
                break;
            case STARTED:
                onAdStarted(ad);
                break;
            case RESUMED:
                onAdResumed();
                break;
            case PAUSED:
                onAdPaused();
                break;
            case SKIPPED:
                onAdSkipped();
                break;
            case TAPPED:
                onAdTapped(ad);
                break;
            case ALL_ADS_COMPLETED:
                onAllAdsCompleted();
                break;
            default:
                break;
        }
    }

    private void onResumeContentRequested() {
        // Let VideoView controls visibility itself when prepared
        carbonVideoPlayerWithAdPlayback.setVideoVisibility(false);
        carbonVideoPlayerWithAdPlayback.getCarbonVideoPlayer().setDefaultControlsEnabled(true);
        carbonVideoPlayerWithAdPlayback.getAdUiContainer().setVisibility(View.GONE);

        carbonVideoPlayerWithAdPlayback.resumeContentAfterAdPlayback(contentVideoPosition);

        isAdPlaying = false;
    }

    private void onPauseContentRequested() {
        carbonVideoPlayerWithAdPlayback.pauseContentForAdPlayback();

        isAdPlaying = true;
    }

    private void onAdLoaded() {
        if (!isPreRollRequest) {
            onPauseContentRequested();
            carbonVideoPlayerWithAdPlayback.getCarbonVideoPlayer().setVideoVisibility(false);
        }

        if (isPostRollRequest) {
            hasPostRollRequested = true;
        }

        contentVideoPosition = (int) carbonVideoPlayerWithAdPlayback.getCarbonVideoPlayer().getCurrentPosition();
        adsManager.start();
    }

    private void onAdStarted(Ad ad) {
        carbonVideoPlayerWithAdPlayback.getCarbonVideoPlayer().setDefaultControlsEnabled(false);
        carbonAdEventHelper.onAdStarted(ad, adDisplayContainer.getAdContainer());
    }

    private void onAdResumed() {
        isAdPlaying = true;
    }

    private void onAdPaused() {
        isAdPlaying = false;
    }

    private void onAdSkipped() {
        // If is post roll request, then just
        // finish the activity when ad skipped
        if (isPostRollRequest) {
            finish();
        }
    }

    private void onAdTapped(Ad ad) {
        carbonAdEventHelper.onAdTapped(ad);
    }

    private void onAllAdsCompleted() {
        if (adsManager != null) {
            adsManager.destroy();
            adsManager = null;
        }

        for (ViewGroup companionView : companionViews) {
            if (companionView != null) {
                companionView.setVisibility(View.GONE);
            }
        }

        // If is post roll request, then just finish the activity
        // instead of trying to resume content video
        if (isPostRollRequest) {
            finish();
        } else {
            onResumeContentRequested();
        }
    }

    @Subscribe
    public void onResumeContentVideoEvent(ResumeContentVideoEvent event) {
        onResumeContentRequested();
    }

    private void finish() {
        if (context != null) {
            ((Activity) context).finish();
        }
    }

    static final class Builder {
        final Context context;
        CarbonVideoPlayerWithAdPlayback carbonVideoPlayerWithAdPlayback;
        List<ViewGroup> companionViews;
        String language;

        Builder(Context context, View rootView) {
            this.context = context;
            companionViews = new ArrayList<>(CarbonCompanion.VALUES.size());
            for (CarbonCompanion companion : CarbonCompanion.VALUES) {
                companionViews.add((ViewGroup) rootView.findViewById(companion.getLayoutId()));
            }
        }

        Builder videoPlayerWithAdPlayback(CarbonVideoPlayerWithAdPlayback carbonVideoPlayerWithAdPlayback) {
            this.carbonVideoPlayerWithAdPlayback = carbonVideoPlayerWithAdPlayback;
            return this;
        }

        Builder language(String language) {
            this.language = language;
            return this;
        }

        CarbonVideoPlayerController build() {
            return new CarbonVideoPlayerController(this);
        }
    }
}
