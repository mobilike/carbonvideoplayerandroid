# How to Use Carbon Video Player (Android) in your apps
Carbon Video Player is a standalone video player library that relies on Google IMA and Play Services Ads SDKs.

## Adding to your project
You can download the repository from the `Downloads` section.

Click `File->New->Import Module` on Android Studio and select the library folder. Set module name to `videoPlayerLib`. Android Studio will automatically import the module and add required lines to `settings.gradle` file. Then update your dependencies in your app's `build.gradle` file:
```groovy
dependencies {
    compile project(':videoPlayerLib')
}
```

## Initializing the library
Init the library in your application's `onCreate()` method:
```java
@Override
public void onCreate() {
    super.onCreate();
    // You can also pass your string resource id as a second argument
    CarbonVideoPlayerManager.init(this, "YOUR_INTERSTITIAL_AD_UNIT_ID");
}
```

If you don't have an application class, we highly recommend to create one and add to your `AndroidManifest.xml` file. You can also call this `init()` method in your launcher activity's `onCreate()`, if you prefer.

## Playing video
In order to play video, you have to use `CarbonVideoActivity` with the provided `start()` method. It has two arguments `Context` and `CarbonVideoConfig`. You can create `CarbonVideoConfig` with its builder pattern. A sample `CarbonVideoConfig` can be created as follows:
```java
CarbonVideoConfig videoConfig = CarbonVideoConfig.builder("YOUR_VIDEO_URL")
                .customParams(customParams)
                .videoShareUrl("YOUR_VIDEO_SHARE_URL")
                .preRollTag("YOUR_PRE_ROLL_TAG")
                .midRollTag("YOUR_MID_ROLL_TAG", midRollPeriodInMinutes)
                .postRollTag("YOUR_POST_ROLL_TAG")
                .build();
```

| Parameter              | Type                | Explanation                                                                          |
|------------------------|---------------------|--------------------------------------------------------------------------------------|
| videoUrl               | String              | Remote video url                                                                     |
| videoShareUrl          | String              | The website that contains the video, might be empty                                  |
| customParams           | Map<String, String> | Overrides `cust_params` parameter of DFP request                                     |
| preRollTag             | String              | Pre roll publisher tag                                                               |
| midRollTag             | String              | Mid roll publisher tag                                                               |
| midRollPeriodInMinutes | int                 | Mid roll ad request period in minutes, must be provided if `midRollTag` is not empty |
| postRollTag            | String              | Post roll publisher tag                                                              |

After you created `CarbonVideoConfig`, you can start `CarbonVideoActivity` in your activity:
```java
CarbonVideoActivity.start(this, videoConfig);
```
